<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    function addUser (Request $request) {
        User::addPerson($request->name, $request->password);
        return Redirect::to("login")->withSuccess('Great! You have Successfully Registererd');
    }

    function checkUser(Request $request) {
        $users = User::checkPerson($request->name, $request->password);
        if($users == '1') { return redirect()->back()->with('alert', 'Success'); }
        else {  return redirect()->back()->with('alert', 'Fail'); }
    }
}
