<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    public static function addPerson($user, $password)
    {
        DB::table('users')->insert(
            ['name' => $user, 'password' => $password]
        );
    }

    public static function checkPerson($user, $password)
    {
        $users = DB::table('users')->where([
            ['name', '=', $user],
            ['password', '=', $password],
        ])->first();
        if ($users) {
            return '1';
        } else {
            return '0';
        }
    }
}
